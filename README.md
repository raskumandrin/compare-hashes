Given the following deep/complex hash structures, write a subroutine that can:

1. Take two hashes as input and will compare one to the other.

2. When hashA is compared to hashC, will return 1 when the keys and values in the structure of hashA are found in hashC

3. When hashB is compared to hashC, will return 0 when the structure in hashB are not found in hashC.

```
$hashA = {
    ‘1’ => ‘red’,
     ‘4’ => {
         ‘b’ =>’yellow’,
    },
    ‘6’ => [
        {
            ‘x’=>’black’,
        },
    ],
}

$hashB = {
     ‘3’ => ‘violet’,
     ‘4’ => {
        ‘a’ =>’red’,
    },
    ‘6’ => [
        {
            ‘x’=>’brown’,
            ‘y’=>’6’,
        },
    ],
}

$hashC = {
     ‘1’ => ‘red’,
     ‘2’ => ‘blue’,
     ‘3’ => ‘green’,
     ‘4’ => {
         ‘a’ =>’purple’,
         ‘b’ =>’yellow’,
     },
     ‘5’ => ‘orange’,
     ‘6’ => [
        {
             ‘x’=>’black’,
             ‘y’=>’5’,
        },
        {
             ‘x’=>’brown’,
             ‘y’=>’5’,
        },
    ],
}
```
