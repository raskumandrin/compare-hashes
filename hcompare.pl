#!/usr/bin/perl

use strict;

my $hashA = {
    '1' => 'red',
    '4' => {
         'b' =>'yellow',
    },
    '6' => [
        {
            'x'=>'black',
        },
    ],
};

my $hashB = {
     '3' => 'violet',
     '4' => {
        'a' =>'red',
    },
    '6' => [
        {
            'x'=>'brown',
            'y'=>'6',
        },
    ],
};

my $hashC = {
     '1' => 'red',
     '2' => 'blue',
     '3' => 'green',
     '4' => {
         'a' =>'purple',
         'b' =>'yellow',
     },
     '5' => 'orange',
     '6' => [
        {
             'x'=>'black',
             'y'=>'5',
        },
        {
             'x'=>'brown',
             'y'=>'5',
        },
    ],
};


sub is_subset_hash {
	my ($a,$b) = @_;
	
	return 0 if ref($a) ne 'HASH';
	return 0 if ref($b) ne 'HASH';
	
	# check this level of keys. every keys from $a must be present in $b
	foreach (keys(%$a)) {
		return 0 unless exists $b->{$_};
	}

	# when value of key is hash run check in recursion
	
	foreach (keys(%$a)) {
		
		# check values
		if (ref($a->{$_}) eq '') {
			return 0 if ref($b->{$_}) ne '';
			return 0 if $a->{$_} ne $b->{$_};
		}
		
		if (ref($a->{$_}) eq 'HASH') {
			return 0 if ref($b->{$_}) ne 'HASH';
			return 0 unless is_subset_hash( $a->{$_}, $b->{$_} );
		}
		
		if (ref($a->{$_}) eq 'ARRAY') {
			return 0 if ref($b->{$_}) ne 'ARRAY';
			# find elements which should be checked in reqursion
			my $subset_in_array = 0;
			foreach my $elem_a (@{$a->{$_}}) {
				foreach my $elem_b (@{$b->{$_}}) {
					$subset_in_array = 1 if is_subset_hash( $elem_a,$elem_b );
				}
			}
			return 0 unless $subset_in_array;
		}
	}
	
	return 1;
}

printf("hashA * hashB: %s\n", is_subset_hash($hashA,$hashB) );

printf("hashA * hashC: %s\n", is_subset_hash($hashA,$hashC) );

printf("hashC * hashA: %s\n", is_subset_hash($hashC,$hashA) );



__END__

Given the following deep/complex hash structures, write a subroutine that can:
1. Take two hashes as input and will compare one to the other.
2. When hashA is compared to hashC, will return 1 when the keys and values in the structure of hashA are found in hashC
3. When hashB is compared to hashC, will return 0 when the structure in hashB are not found in hashC.
https://bitbucket.org/raskumandrin/compare-hashes/overview
stas@raskumandrin.ru



